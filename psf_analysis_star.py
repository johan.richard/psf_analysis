from astropy.io import fits
from scipy import optimize
import numpy as np
import matplotlib as mpl
mpl.use('pgf')
import argparse

from matplotlib.backends.backend_pgf import FigureCanvasPgf
pgf_with_custom_preamble = {
    'font.family': 'serif', # use serif/main font for text elements
    'font.size': 12,
    'text.usetex': True,    # use inline math for ticks
    'pgf.rcfonts': False,   # don't setup fonts from rc parameters
    'pgf.texsystem': 'pdflatex',
    'pgf.preamble': [
        r'\usepackage{graphics}',
        r'\pdfpageattr{/Group <</S /Transparency /I true /CS /DeviceRGB>>}']
                        }

mpl.rcParams.update(pgf_with_custom_preamble)
mpl.backend_bases.register_backend('eps', FigureCanvasPgf)
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.path import Path
from matplotlib import patches
from matplotlib import colors as mpl_colors
from matplotlib import cm as cmx
from matplotlib import transforms


if __name__ == '__main__':


   #input_table='psf_test.fits'   #USE ARGV instead
   #starid=str(96403) #USE ARGV instead

   parser = argparse.ArgumentParser(description='MUSE PSF analysis script for single stars')
   parser.add_argument('psf_file', type=str, help='PSF file output from PampelMuse',action="store")
   parser.add_argument('starid', type=str, help='star ID for analysis',action="store")
   parser.add_argument('psf_file2', nargs='?', help='optional second PSF file output from PampelMuse',type=str, default=None)
   parser.add_argument('starid2', nargs='?', help='optional second star ID for comparison',type=str, default=None)
   parser.add_argument('--nlplot',help='number of wavelength bins to display (default 4)',
                       type=int, action="store",dest="nlplot",default=6)

   args=parser.parse_args()
   input_table=args.psf_file
   starid=args.starid
   nlplot=args.nlplot
   print "Main results file="+args.psf_file+" - star ID="+starid

   input_table2=None
   flagcomp=False
   if((args.psf_file2 is not None) and (args.starid2 is not None)):
       flagcomp=True
       input_table2=args.psf_file2
       starid2=args.starid2
       print "Comparison file="+input_table2+" - star ID="+starid2

   h=fits.open(input_table)
   if(input_table[-5:]=='.fits'):
       input_table=input_table[:-5]
   if(input_table[-4:]=='.psf'):
       input_table=input_table[:-4]
   listr=h[0].data*0.2

   if(flagcomp):
       h2=fits.open(input_table2)
       if(input_table2[-5:]=='.fits'):
           input_table2=input_table2[:-5]
       if(input_table2[-4:]=='.psf'):
           input_table2=input_table2[:-4]
       listr2=h2[0].data*0.2

   startl=h['PM_'+starid].header['CRVAL1']*1e10
   stepl=h['PM_'+starid].header['CDELT1']*1e10
   nl=h['PM_'+starid].data.shape[0]

   #make spatial comparison plot
   fig = plt.figure(figsize=(4.,4.), tight_layout=True)
   gs = gridspec.GridSpec(1,1)

   ax=fig.add_subplot(gs[0],adjustable='datalim')
   cNorm=mpl_colors.Normalize(vmin=startl,vmax=startl+(nl-1)*stepl)
   scalarMap=cmx.ScalarMappable(norm=cNorm,cmap='RdYlBu_r')

   profiles=h['PR_'+starid].data

   for lslice in range(nl):
      curwl=startl+lslice*stepl
      colorVal=scalarMap.to_rgba(curwl)
      ax.plot(listr,profiles[:,lslice]/profiles[0,lslice],c=colorVal,lw=0.2)
   ax.set_yscale('log')
   ax.set_xlabel('radius (arcsec)',size=16)
   ax.set_ylabel('flux',size=16)
   ax.set_title(input_table+' star='+starid+'(solid)')

   outfile='Profile_'+input_table+'_'+starid
   if(flagcomp and input_table2 is not None):
        profiles=h2['PR_'+starid2].data
        ax.plot(listr2,profiles[:,0]/profiles[0,0],'--',c=scalarMap.to_rgba(4800.0),lw=1)
        ax.plot(listr2,profiles[:,-1]/profiles[0,-1],'--',c=scalarMap.to_rgba(9300.0),lw=1)
        outfile=outfile+'_vs_'+input_table2+'_'+starid2
        ax.set_title(input_table+' star='+starid+'(solid)\n'+input_table2+' star='+starid2+'(dashed)')
   outfile=outfile+'.pdf'
  
   ax.set_ybound(0.0005,1.1)
   ax.text(8.,0.8,'4800 A',color=scalarMap.to_rgba(4800.0),size=16)
   ax.text(8.,0.3,'9300 A',color=scalarMap.to_rgba(9300.0),size=16)
        
   fig.savefig(outfile, dpi=600)
   plt.show()
   #if(not flagcomp):
   #    print "RESIDUALS HERE"

   vsize=6.0
   nrows=2
   if(flagcomp):
       vsize=12.0
       nrows=4

   fig= plt.figure(figsize=(3.*nlplot,vsize), tight_layout=True)
   gs = gridspec.GridSpec(nrows,nlplot)

   cubedat=h['DAT_'+starid].data
   cuberes=h['RES_'+starid].data
   model=cubedat-cuberes

   nl=h['DAT_'+starid].data.shape[0]
   for ilambda in range(0,nlplot):
      lslice=ilambda*(nl-1)/(nlplot-1)
      curwl=startl+lslice*stepl
      ax=fig.add_subplot(gs[ilambda],aspect='equal')
      ax.imshow(cubedat[lslice,:,:])
      ax.get_xaxis().set_visible(False)
      ax.get_yaxis().set_visible(False)
      ax.set_title(str(int(curwl)),size=8)
      levels = np.array([0.02,0.04,0.08,0.16,0.32,0.64])
      levels = levels*np.max(model[lslice,:,:])
      ax.contour(model[lslice,:,:], levels,colors='white',linewidths=0.1)
      ax=fig.add_subplot(gs[ilambda+nlplot],aspect='equal')
      ax.imshow(cuberes[lslice,:,:])
      ax.get_xaxis().set_visible(False)
      ax.get_yaxis().set_visible(False)
      ax.set_title(str(int(curwl)),size=8)

   outfile='Residuals_'+input_table+'_'+starid
   if(flagcomp):
      cubedat=h2['DAT_'+starid2].data
      cuberes=h2['RES_'+starid2].data
      model=cubedat-cuberes

      startl=h2['PM_'+starid2].header['CRVAL1']*1e10
      stepl=h2['PM_'+starid2].header['CDELT1']*1e10
      nl=h2['DAT_'+starid2].data.shape[0]
      for ilambda in range(0,nlplot):
         lslice=ilambda*(nl-1)/(nlplot-1)
         curwl=startl+lslice*stepl
         ax=fig.add_subplot(gs[ilambda+2*nlplot],aspect='equal')
         ax.imshow(cubedat[lslice,:,:])
         ax.get_xaxis().set_visible(False)
         ax.get_yaxis().set_visible(False)
         ax.set_title(str(int(curwl)),size=8)
         levels = np.array([0.02,0.04,0.08,0.16,0.32,0.64])
         levels = levels*np.max(model[lslice,:,:])
         ax.contour(model[lslice,:,:], levels,colors='white',linewidths=0.1)
         ax=fig.add_subplot(gs[ilambda+3*nlplot],aspect='equal')
         ax.imshow(cuberes[lslice,:,:])
         ax.get_xaxis().set_visible(False)
         ax.get_yaxis().set_visible(False)
         ax.set_title(str(int(curwl)),size=8)

      outfile=outfile+'_vs_'+input_table2+'_'+starid2
   outfile=outfile+'.pdf'
   fig.savefig(outfile,dpi=600)
   plt.show()

