from astropy.io import fits
from scipy import optimize
import numpy as np
import matplotlib as mpl
import argparse


mpl.use('pgf')

from matplotlib.backends.backend_pgf import FigureCanvasPgf
pgf_with_custom_preamble = {
    'font.family': 'serif', # use serif/main font for text elements
    'font.size': 8,
    'text.usetex': True,    # use inline math for ticks
    'pgf.rcfonts': False,   # don't setup fonts from rc parameters
    'pgf.texsystem': 'pdflatex',
    'pgf.preamble': [
        r'\usepackage{graphics}',
        r'\pdfpageattr{/Group <</S /Transparency /I true /CS /DeviceRGB>>}']
                        }

mpl.rcParams.update(pgf_with_custom_preamble)
mpl.backend_bases.register_backend('eps', FigureCanvasPgf)
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.path import Path
from matplotlib import patches
from matplotlib import colors as mpl_colors
from matplotlib import transforms

def eemoffat(fwhm,beta):
   X,Y=np.meshgrid(np.arange(-0.1,0.1,0.001),np.arange(-0.1,0.1,0.001))
   rd=fwhm/(2*np.sqrt(2.0**(1.0/beta)-1))
   moffat = lambda p, q: (beta-1)*(1 + (p**2+q**2)/(rd**2.0))**(-beta)/(np.pi*rd**2.0)
   return(np.sum(moffat(X,Y)*0.001*0.001))


#make spatial comparison plot
def spatial_plot(fig,h,nlplot,size):
   startl=h[1].header['CRVAL1']*1e10
   stepl=h[1].header['CDELT1']*1e10
   if(len(h[-1].data.shape)>2):
      nstars=(len(h)-1)/4
   else:
      nstars=(len(h)-1)/2
   nl=h[1].data.shape[0]

   gs = gridspec.GridSpec(2,nlplot)

   fmin=np.nan
   fmax=np.nan
   for i in range(nstars):
       starid=h[i+1].name[3:]
       fmin=np.nanmin([fmin,np.nanmin(h['PM_'+starid].data['fwhm'])*size])
       fmax=np.nanmax([fmax,np.nanmax(h['PM_'+starid].data['fwhm'])*size])

   betamin=1.7
   betamax=2.7

   for ilambda in range(0,nlplot):
      lslice=ilambda*(nl-1)/(nlplot-1)
      curwl=startl+lslice*stepl
      x=[]
      y=[]
      fwhm=[]
      snr=[]
      label=[]
      e=[]
      theta=[]
      beta=[]
      for i in range(nstars):
         starid=h[i+1].name[3:]
         #plt.plot(h['PM_'+starid].data[7,lslice],h['PM_'+starid].data[6,lslice],'o')
         snr.append(h['PM_'+starid].data['snr'][lslice]/10.0)
         beta.append(h['PM_'+starid].data['beta'][lslice])
         try:
           e.append(h['PM_'+starid].data['e'][lslice]*10.0)
           theta.append(h['PM_'+starid].data['theta'][lslice]) #*np.pi/180.0)
           plotell=True
         except:
           plotell=False
         fwhm.append(h['PM_'+starid].data['fwhm'][lslice]*size)
         x.append(h['PM_'+starid].data['xc'][lslice])
         y.append(h['PM_'+starid].data['yc'][lslice])
         label.append(starid)

      ax=fig.add_subplot(gs[ilambda],aspect='equal', adjustable='datalim')
      ax.set_xbound(0.0,400.0)
      ax.set_ybound(0.0,400.0)
      s = ax.scatter(x, y, c=fwhm, cmap='RdYlBu_r', s=snr, edgecolors='black',lw=0.5,vmin=fmin,vmax=fmax)

      if(plotlabels):
        try:
           for i in range(nstars):
              if(x[i]>0)and(y[i]>0):
                 ax.text(x[i]-5.0,y[i]+15.0,label[i])
        except:
           continue

      if(plotell):
        try:
           for i in range(nstars):
              if(x[i]>0)and(y[i]>0):
                 ax.plot([x[i]-20.0*e[i]*np.cos(theta[i]),x[i]+20.0*e[i]*np.cos(theta[i])],[y[i]-20.0*e[i]*np.sin(theta[i]),y[i]+20.0*e[i]*np.sin(theta[i])],color='black',lw=1.0)
        except:
           continue

      if(ilambda==nlplot-1):
          plt.colorbar(s, ax=ax)

      #ax.set_xlabel('x')
      ax.set_ylabel('y')
      try:
         ax.set_title(h.filename()+'\nFWHM='+str(int(100.0*np.nanmedian(fwhm))/100.0)+'+/-'+str(int(100.0*np.nanstd(fwhm))/100.0)+' L='+str(int(curwl))+' A')
      except:
         continue

      ax=fig.add_subplot(gs[ilambda+nlplot],aspect='equal', adjustable='datalim')
      ax.set_xbound(0.0,400.0)
      ax.set_ybound(0.0,400.0)
      s = ax.scatter(x, y, c=beta, cmap='RdYlBu_r', s=snr,  edgecolors='black',lw=1,vmin=betamin,vmax=betamax)

      if(plotlabels):
        try:
           for i in range(nstars):
              if(x[i]>0)and(y[i]>0):
                 ax.text(x[i]-5.0,y[i]+15.0,label[i])
        except:
           continue

      #if(plotell):
      if(ilambda==nlplot-1):
          plt.colorbar(s, ax=ax)

      ax.set_xlabel('x')
      ax.set_ylabel('y')
      #ax.set_title('Beta L='+str(int(curwl))+' A')
      ax.set_title(h.filename()+'\nBeta='+str(int(100.0*np.nanmedian(beta))/100.0)+'+/-'+str(int(np.nanstd(beta)*100.0)/100.0)+' L='+str(int(curwl))+' A')


#make EE comparison plot
def ee_plot(fig,h,h2,size1,size2):
   if(h2):
        gs = gridspec.GridSpec(1,2)
   else:
        gs = gridspec.GridSpec(1,1)
   if(len(h[-1].data.shape)>2):
      nstars=(len(h)-1)/4
   else:
      nstars=(len(h)-1)/2
   startl=h[1].header['CRVAL1']*1e10
   stepl=h[1].header['CDELT1']*1e10
   if(h2):
     startl2=h2[1].header['CRVAL1']*1e10
     stepl2=h2[1].header['CDELT1']*1e10
     kwl2=int((7500.0-startl2)/stepl2)  #7500 Angstroms in second psf file

   kwl1=int((7500.0-startl)/stepl)  #7500 Angstroms in first psf file

   eelist1=[]
   eelist2=[]
   gainlist=[]
   labellist=[]

   for i in range(nstars):
       starid=h[i+1].name[3:]
       fwhm1=h['PM_'+starid].data['fwhm'][kwl1]*size1
       beta1=h['PM_'+starid].data['beta'][kwl1]
       if(h2):
          try:
             fwhm2=h2['PM_'+starid].data['fwhm'][kwl2]*size2
             beta2=h2['PM_'+starid].data['beta'][kwl2]
             ee1=eemoffat(fwhm1,beta1)
             ee2=eemoffat(fwhm2,beta2)
             if(not np.isnan(ee1) and not np.isnan(ee2)):
               eelist1.append(ee1)
               eelist2.append(ee2)
               gainlist.append(ee1/ee2)
               labellist.append(starid)
          except:
             print starid+' not found or bad fit'
       else:
          if(fwhm1 is not np.nan):
            eelist1.append(eemoffat(fwhm1,beta1))
            labellist.append(starid)

   ax=fig.add_subplot(gs[0],adjustable='datalim')
   ax.plot(eelist1,'o',color='red')
   if(h2):
       ax.plot(eelist2,'s',color='black')
   ax.set_xlabel('star')
   ax.set_ylabel('EE')
   if(h2):
      ax.set_title('EE1='+str(int(1000.0*np.nanmedian(eelist1))/1000.0)+'+/-'+str(int(1000.0*np.nanstd(eelist1))/1000.0)+'\n'+
                   'EE2='+str(int(1000.0*np.nanmedian(eelist2))/1000.0)+'+/-'+str(int(1000.0*np.nanstd(eelist2))/1000.0))
   else:
      ax.set_title('EE='+str(int(1000.0*np.nanmedian(eelist1))/1000.0)+'+/-'+str(int(1000.0*np.nanstd(eelist1))/1000.0))

   if(plotlabels):
       for x,y,txt in zip(np.arange(len(eelist1)),np.array(eelist1),labellist):
           ax.text(x,y,txt,va='bottom',ha='left')

   if(h2):
      ax=fig.add_subplot(gs[1],adjustable='datalim')
      ax.plot(gainlist,'o',color='blue')
      if(plotlabels):
         for x,y,txt in zip(np.arange(len(gainlist)),np.array(gainlist),labellist):
             ax.text(x,y,txt,va='bottom',ha='left')

      ax.set_xlabel('star')
      ax.set_ylabel('gain in EE')
      ax.set_title('GAIN='+str(int(100.0*np.nanmedian(gainlist))/100.0)+'+/-'+str(int(100.0*np.nanstd(gainlist))/100.0))


#make radial profile comparison plot
def radial_plot(fig,gs,h,nlplot,startplot,size):
   startl=h[1].header['CRVAL1']*1e10
   stepl=h[1].header['CDELT1']*1e10
   if(len(h[-1].data.shape)>2):
      nstars=(len(h)-1)/4
   else:
      nstars=(len(h)-1)/2
   nl=h[1].data.shape[0]

   #Clean negative PR values
   for i in range(nstars):
       starid=h[i+1].name[3:]
       ksel=np.where(h['PR_'+starid].data<1e-4)
       h['PR_'+starid].data[ksel]=1e-4

   returnval=[]
   axreturn=[]
   for ilambda in range(0,nlplot):
      allval=[]
      lslice=ilambda*(nl-1)/(nlplot-1)
      curwl=startl+lslice*stepl
      ax=fig.add_subplot(gs[ilambda+startplot],adjustable='datalim')
      ax.set_yscale('log')
      listr=h[0].data*size
      for i in range(nstars):
         starid=h[i+1].name[3:]
         ax.plot(listr,h['PR_'+starid].data[:,lslice]/h['PR_'+starid].data[0,lslice],color='gray',lw=0.4)
      for r in range(listr.shape[0]):
         flux=[]
         for i in range(nstars):
             starid=h[i+1].name[3:]
             flux.append(h['PR_'+starid].data[r,lslice]/h['PR_'+starid].data[0,lslice])
         ax.plot(listr[r],np.nanmedian(flux),'o',color='red')
         ax.errorbar(listr[r],np.nanmedian(flux),np.nanstd(flux),color='red')
         allval.append(np.nanmedian(flux))

      
      ax.set_yscale('log')
      ax.set_ybound(0.0005,1.1)
      ax.set_xlabel('radius',size=16)
      ax.set_ylabel('flux',size=16)
      ax.set_title(h.filename()+'\nL='+str(int(curwl))+' A')
      returnval.append(allval)
      axreturn.append(ax)

      ax=fig.add_subplot(gs[ilambda+startplot+nlplot],adjustable='datalim')
      residuals=np.array(listr)*0.0
      navg=0.0

      thinrange=np.arange(0,np.max(listr),0.01)
      for i in range(nstars):
          starid=h[i+1].name[3:]
          fwhm=h['PM_'+starid].data['fwhm'][lslice]*size
          beta=h['PM_'+starid].data['beta'][lslice]
          flux=h['PM_'+starid].data['flux'][lslice]
          rd=fwhm/(2.0*np.sqrt(2.0**(1.0/beta)-1.0))
          moffat = lambda p : flux*(beta-1.0)*(1.0 + (p**2)/(rd**2.0))**(-beta)/(np.pi*rd**2.0)
          thinmoffat=moffat(thinrange)

          finefit=[]
          for r in listr:
              finefit.append(thinmoffat[np.argmin(abs(thinrange-r))])
          finefit=np.array(finefit)
          #finefit2=moffat(listr)

          res=(h['PR_'+starid].data[:,lslice]/size/size-finefit)/finefit
          ax.plot(listr,res,'--',color='gray',lw=0.4)
          if(not np.isnan(np.sum(res))):
              residuals+=res
              navg+=1.0

      residuals/=navg
      ax.plot(listr,residuals,'s-',color='red')
      ax.set_ybound(-1.0,1.0)

   return axreturn,returnval


#make spectral comparison plot
def spectral_plot(fig,gs,h,kmin,kmax,startplot,size):
   startl=h[1].header['CRVAL1']*1e10
   stepl=h[1].header['CDELT1']*1e10
   nl=h[1].data.shape[0]
   if(len(h[-1].data.shape)>2):
      nstars=(len(h)-1)/4
   else:
      nstars=(len(h)-1)/2


   axf=fig.add_subplot(gs[startplot])
   axb=fig.add_subplot(gs[startplot+1])
   for i in range(nstars):
       starid=h[i+1].name[3:]
       axf.plot(startl+np.arange(0,nl)*stepl,h['PM_'+starid].data['fwhm']*size,color='gray',lw=0.4)
       axb.plot(startl+np.arange(0,nl)*stepl,h['PM_'+starid].data['beta'],color='gray',lw=0.4)

   wlist=[]
   fwhmlist=[]
   fwhmerrlist=[]
   betalist=[]
   for lslice in range(0,nl):
     if((lslice<kmin)or(lslice>kmax)):
       curwl=startl+lslice*stepl
       fwhm=[]
       beta=[]
       for i in range(nstars):
         starid=h[i+1].name[3:]
         fwhm.append(h['PM_'+starid].data['fwhm'][lslice]*size)
         beta.append(h['PM_'+starid].data['beta'][lslice])

       axf.plot(curwl,np.nanmedian(fwhm),'o',color='red')
       axf.errorbar(curwl,np.nanmedian(fwhm),np.nanstd(fwhm),color='red')
       axb.plot(curwl,np.nanmedian(beta),'o',color='red')
       axb.errorbar(curwl,np.nanmedian(beta),np.nanstd(beta),color='red')
       wlist.append(curwl)
       fwhmlist.append(np.nanmedian(fwhm))
       fwhmerrlist.append(np.nanstd(fwhm))
       betalist.append(np.nanmedian(beta))
         
       axf.set_ylabel('FWHM')
       axb.set_xlabel('Wavelength')
       axb.set_ylabel('beta')

   fitfunc=lambda p, xx: p[0]*(np.array(xx)/7500.0)**p[1] #target function
   errfunc=lambda p, xx, yy, ww: ww*(fitfunc(p,xx)-yy) #weighted error function (distance to the target function)

   p0=[fwhmlist[nl/2],-0.2] # initial guess

   p1,success=optimize.leastsq(errfunc,p0[:],args=(wlist,fwhmlist,fwhmerrlist))
   wfulllist=np.linspace(startl,np.max(wlist),1000)
   axf.plot(wfulllist,fitfunc(p1,wfulllist),'b-',lw=3.0)

   #compute EE @ 7500 Angstroms
   eeg=eemoffat(fitfunc(p1,wfulllist[np.argmin(np.abs(wfulllist-7500.0))]),betalist[np.argmin(np.abs(np.array(wlist)-7500.0))])

   try:
      axf.set_title(h.filename()+'\nFWHM7500='+str(int(100*p1[0])/100.0)+' n='+str(int(100.0*p1[1])/100.0)+'\nEE7500='+str(int(1000.0*eeg)/1000.0))
   except:
      print "bad fit"

   return axf,axb,wfulllist,fitfunc(p1,wfulllist),eeg



if __name__ == '__main__':

   # init command line parser
   parser = argparse.ArgumentParser(description='MUSE PSF analysis script')
   parser.add_argument('psf_file', type=str, help='PSF file output from PampelMuse',action="store")

   parser.add_argument('--nlplot',help='number of wavelength bins to display',
                       type=int, action="store",dest="nlplot",default=4)
   parser.add_argument('--size1',help='number of wavelength bins to display',
                       type=float, action="store",dest="size1",default=0.2)
   parser.add_argument('--size2',help='number of wavelength bins to display',
                       type=float, action="store",dest="size2",default=0.2)
   parser.add_argument('--hidelabels', help='hide star labels', action="store_false",default=True)
   parser.add_argument('--hideell', help='hide ellipticity sticks', action="store_false",default=True)
   parser.add_argument('--ao', help='adaptive optics', action="store_true",default=False)

   parser.add_argument('psf_file2', nargs='?', help='optional second PSF file output from PampelMuse',type=str, default=None)

   ninput=1

   # parse arguments
   args = parser.parse_args()
   size1=args.size1
   size2=args.size2

   input_table=args.psf_file
   nlplot=args.nlplot
   plotlabels=args.hidelabels
   plotell=args.hideell
   notch=args.ao

   print "Opening "+input_table 
   h=fits.open(input_table)
   h2=None

   if(args.psf_file2 is not None):
       print "Opening second file="+args.psf_file2
       input_table2=args.psf_file2
       h2=fits.open(input_table2)
       ninput=2

   kmax=-1
   kmin=h[1].data.shape[0]
   startl=h[1].header['CRVAL1']*1e10
   stepl=h[1].header['CDELT1']*1e10
   if(len(h[-1].data.shape)>2):
      nstars=(len(h)-1)/4
   else:
      nstars=(len(h)-1)/2

   #notch
   if notch:
      #ignore 5805 5966
      kmin=int((5805-startl)/stepl)
      kmax=int((6020-startl)/stepl)
      for i in range(nstars):
          starid=h[i+1].name[3:]
          h['PM_'+starid].data['fwhm'][kmin:kmax]=np.nan
          h['PM_'+starid].data['beta'][kmin:kmax]=np.nan
          h['PR_'+starid].data[:,kmin:kmax]=np.nan
      if(ninput>1):
          if(len(h2[-1].data.shape)>2):
              nstars2=(len(h2)-1)/4
          else:
              nstars2=(len(h2)-1)/2
          for i in range(nstars):
              starid=h2[i+1].name[3:]
              h2['PM_'+starid].data['fwhm'][kmin:kmax]=np.nan
              h2['PM_'+starid].data['beta'][kmin:kmax]=np.nan
              h2['PR_'+starid].data[:,kmin:kmax]=np.nan


   fig = plt.figure(figsize=(4.*nlplot,8.), tight_layout=True)
   spatial_plot(fig,h,nlplot,size1)
   fig.savefig('FWHM_Beta_spatial.pdf', dpi=600)
   plt.show()

   if(ninput>1):
      fig = plt.figure(figsize=(4.*nlplot,8.), tight_layout=True)
      spatial_plot(fig,h2,nlplot,size2)
      fig.savefig('FWHM_Beta_spatial2.pdf', dpi=600)
      plt.show()

   fig=plt.figure(figsize=(4.*ninput,4.),tight_layout=True)
   ee_plot(fig,h,h2,size1,size2)
   fig.savefig('EEplot.pdf')
   plt.show()

   fig = plt.figure(figsize=(4.*nlplot,8.*ninput), tight_layout=True)
   gs = gridspec.GridSpec(ninput*2,nlplot)
   ax1,prof1=radial_plot(fig,gs,h,nlplot,0,size1)
   if(ninput>1):
      ax2,prof2=radial_plot(fig,gs,h2,nlplot,nlplot*2,size2)

      listr=h[0].data*size1
      listr2=h2[0].data*size2
      for i in range(0,nlplot):
          ax1[i].plot(listr2,prof2[i],'s',color='black')
          ax2[i].plot(listr,prof1[i],'s',color='black')
          ax1[i].set_ybound(0.0005,1.1)
          ax2[i].set_ybound(0.0005,1.1)

   fig.savefig('rprofile_all.pdf', dpi=600)
   plt.show()

   fig = plt.figure(figsize=(8,4.*ninput), tight_layout=True)
   gs = gridspec.GridSpec(2,ninput)
   axf1,axb1,wfit1,yfit1,ee1=spectral_plot(fig,gs,h,kmin,kmax,0,size1)
   if(ninput>1):
       axf2,axb2,wfit2,yfit2,ee2=spectral_plot(fig,gs,h2,kmin,kmax,2,size2)
       axf2.plot(wfit1,yfit1,'-.',color='black',lw=2.0)
       axf1.plot(wfit2,yfit2,'-.',color='black',lw=2.0)
       fmin=np.min([np.min(axf1.get_ylim()),np.min(axf2.get_ylim())])
       fmax=np.max([np.max(axf1.get_ylim()),np.max(axf2.get_ylim())])
       axf1.set_ylim(fmin,fmax)
       axf2.set_ylim(fmin,fmax)
       bmin=np.min([np.min(axb1.get_ylim()),np.min(axb2.get_ylim())])
       bmax=np.max([np.max(axb1.get_ylim()),np.max(axb2.get_ylim())])
       axb1.set_ylim(bmin,bmax)
       axb2.set_ylim(bmin,bmax)

   fig.text(0.5,0.95,"Gain in EE="+str(int(100.0*ee1/ee2)/100.0))
   fig.savefig('FWHM_Beta_wavelength.pdf', dpi=600)
   plt.show()

